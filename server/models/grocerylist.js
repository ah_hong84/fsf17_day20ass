//to tell this project than this module will be and can be used by other .js file
module.exports = function( conn, Sequelize ){
    var GroceryList = conn.define("grocery_list", {
        id: {
            type: Sequelize.INTEGER(11),
            primaryKey: true,
            allowNull: false
        },
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false          
        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },{
        tableName: "grocery_list",
        timestamps: false
    });
    return GroceryList; //returning the whole GroceryList object
};