//loading the libraries
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

//setup the port
const NODE_PORT = process.env.NODE_PORT || 3000;

//setup the path
const CLIENT_FOLDER = path.join(__dirname, "../client");
const MSG_FOLDER = path.join(CLIENT_FOLDER, "assets/messages");
const API_GROCERYLIST_ENDPOINT = "/api/grocerylist";

//declare and set MySQL username and password as a constant
const MYSQL_USERNAME = "root"; 
const MYSQL_PASSWORD = "hong";

//create instance for express app
var app = express();

//create instance for sequelize app
var conn = new Sequelize(
    "grocery_list",
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: "localhost",
        logging: console.log,
        dialect: "mysql",
        pool: {
            max: 5,         //maximum only 5 connection can access the server at the sametime
            min: 0,         //minimum connection is 0
            idle: 10000
        }
    }
);

//importing my own app module (GroceryList module) from folder /server/grocerylist.js
//using input of "connection and Sequelize", which is a new set of input, and is different from what is shown in /server/grocerylist.js
//this is like creating a new instance of GroceryList using /server/grocerylist.js
var GroceryList = require("./models/grocerylist")(conn, Sequelize);


//setup of the configuration of express
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json()); //telling express that it's a bodyParser JSON, if never set this commend, there will be no JSON body
app.use(bodyParser.urlencoded({extended : false})); //disable to default, force server to only accept JSON

/** 
 *  GET /api/grocerylist --Get all grocerylist records
 *  PUT /api/grocerylist/:id --Update a item record
**/

//Search record from database using some keywords
//route path -- retrieve all brand and desription
app.get(API_GROCERYLIST_ENDPOINT, function(req,res){
    GroceryList
        .findAll({      //we are seaching all field in GroceryList table
            where: {    //SQL condition for "where"
                    $or: [  //SQL condition for "or"
                        { brand: {$like: "%" + req.query.searchString + "%"}},  //we are search using brand column
                        { name: {$like: "%" + req.query.searchString + "%"}}     //we are also searching the name column
                ]
                
            }
        }).then(function(grocerylist){  //if good, do below
            res
                .status(200)
                .json(grocerylist);
        }).catch(function(err){         //if error, do below
            res
                .status(500)
                .json(err);
        })
});

//Update database
app.put(API_GROCERYLIST_ENDPOINT + "/:id", function(req,res){  // "/:id" is defining the field in the table that we want to use as the key to search and then we will update its corrosponding data
    var whereClause = {}; //we declare "where" as a object with nothing inside (initialize object)
    whereClause.brand = req.params.brand; //params is the data that's input by user 
    GroceryList
        .update({upc12: req.body.upc12,
                 brand: req.body.brand,
                 name: req.body.name},    //we are updating the value in brand using the value in req.body.brand
                                                    //body is the data that's input by user that he/she wanna change (e.g the data typed in the Body JSON in POSTMAN)
                    {where: whereClause}                  // it's a sql condition and it means "where brand = req.params.brand"
        ).then(function(result){    //if good, do below
            res.status(200).json(result);
        }).catch(function(err){     //if error, do below
            console.log(err);
        });

});

app.use(function(err, res, req, next){  //if above app.use has error, it will drop into here
    console.log("there's an error");    //we can setup function that we want to handle the error here
    res.status(500).sendFile(path.join(MSG_FOLDER, "500.html"));    //once completed handling the error, it will end here
});

//start the server
app.listen(NODE_PORT, function(){
    console.log("Server is running at port %s", NODE_PORT);
});