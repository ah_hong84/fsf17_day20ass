//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){
    //create an instance of my Angular app
    angular
        .module("GMM")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['GroceryService']; //inject the dependency

    //define the function to be used as controller
    function SearchDBCtrl(GroceryService){ //
        var vm = this;

        vm.searchString = ""; //before starting the search, the data inside searchString is emptuy
        vm.result = null;   //hence the result should be null
        vm.searchBrand = searchBrand;
        
        
        init();

        function init(){ //initializing the object
            searchBrand("");
        };

        function searchBrand(){ //creating a "search" function
            searchItemFromDB(vm.searchString);
        };


        function searchItemFromDB(param) {
            GroceryService
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveGroceryListDB(param)
                .then(function (results) {
                    vm.brand = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }
    }    
})();