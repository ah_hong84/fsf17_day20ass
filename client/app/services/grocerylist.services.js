//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){
    //create an instance of my Angular app
    angular
        .module("GMM")
        .service("GroceryService", GroceryService);

    GroceryService.$inject = ["$http"];

    //define the function to be used as service
    function GroceryService($http){
        var service = this;
        service.retrieveGroceryListDB = retrieveGroceryListDB; //expose retrieveGroceryListDB

        function retrieveGroceryListDB(searchString){ //function when we do the submit on searchDB.html
            return $http({
                method: "GET", //using GET method
                url: "/api/grocerylist", 
                params: {"searchString": searchString} //using the information that's inside the searchString
            });
        }
    }
})();