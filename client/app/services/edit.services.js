//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){
    //create an instance of my Angular app
    angular
        .module("GMM")
        .service("EditService", EditService);

    EditService.$inject = ["$http"];

    //define the function to be used as service
    function EditService($http){
        var service = this;
        service.updateItem = updateItem;

        function updateItem(id){
            return $http({
                method: "PUT",
                url: "/api/grocerylist" + "/:id",
                data: { 
                    upc12: upc12, 
                    brand: brand,
                    name: name
                } 
            });
        }

        
    }
})();