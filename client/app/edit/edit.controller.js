//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){
    //create an instance of my Angular app
    angular
        .module("GMM")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = [ "$filter", "EditService"]; //inject the dependency

    //define the function to be used as controller
    function EditCtrl($filter, EditService){ // "$filter" is so that we can 
        var vm = this;

        //search criteria
        vm.brand ="";
        vm.name ="";
        //result from the ajax endpoint
        vm.result = {};

        // expose all the functions to the view
        vm.initDetails = initDetails;
        vm.updateItem = updateItem;
        vm.search = search;

        function search(){
            initDetails();
            vm.showDetails = true;
            GroceryService
                .retrieveGroceryListDB(vm.searchString)
                .then(function(result){
                    vm.showDetails = true;

                    if(!result.data)
                        return;
                }).catch(function(error){
                    console.log(JSON.stringify(error));
                })
        }

        function initDetails(){
            vm.result.upc12 = "";
            vm.result.brand = "";
            vm.result.name = "";
        }

        function updateItem(){
            EditService
                .updateGroceryList(vm.brand, vm.result.id)
                .then(function(response){
                    console.log(JSON.stringify(response.data));
                }).catch(function(err){
                    console.log(err);
                })
        }
    }

    

})();